package playingwithactors

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout
import playingwithactors.Checker.{BlackListedUser, CheckUser, WhiteListedUser}
import playingwithactors.Recorder.AddUser
import playingwithactors.Storage.StoreUser

import scala.concurrent.duration._

/**
  * Created by andrejs.dasko on 19-Jun-18.
  */
case class User(name: String, email: String)

object Recorder {
  def props(checker: ActorRef, storage: ActorRef) = Props(new Recorder(checker, storage))

  sealed trait RecorderMsg
  case class AddUser(user: User) extends RecorderMsg
}

object Checker {
  def props = Props[Checker]

  sealed trait CheckerMsg
  case class CheckUser(user: User) extends CheckerMsg

  sealed trait CheckerResponse
  case class WhiteListedUser(user: User) extends CheckerResponse
  case class BlackListedUser(user: User) extends CheckerResponse
}

object Storage {
  def props = Props[Storage]

  sealed trait StorageMsg
  case class StoreUser(user: User) extends StorageMsg
}

class Recorder(checker: ActorRef, storage: ActorRef) extends Actor {
  import scala.concurrent.ExecutionContext.Implicits.global

  implicit val timeout = Timeout(5.seconds)

  override def receive: Receive = {
    case AddUser(user) =>
      checker ? CheckUser(user) map {
        case WhiteListedUser(user) =>
          println(s"Recorder: $user is white listed. Storing...")
          storage ! StoreUser(user)
        case BlackListedUser(user) =>
          println(s"Recorder: $user is blacklisted")
      }
  }
}

class Checker extends Actor {

  val blackList = List(User("John", "john.doe@example.com"))

  override def receive: Receive = {
    case CheckUser(user) if blackList.contains(user) =>
        println(s"Checker: $user is blacklisted")
        sender() ! BlackListedUser(user)
    case CheckUser(user) =>
      println(s"Checker: $user is not blacklisted")
      sender() ! WhiteListedUser(user)
  }
}

class Storage extends Actor {

  var users = List.empty[User]

  override def receive: Receive = {
    case StoreUser(user) =>
      users = user :: users
      println(s"Storage: User stored: ${user.email}")
  }
}

object TellAndAsk extends App {
  val system = ActorSystem("tell-and-ask")

  val checker = system.actorOf(Checker.props, "checker")

  val storage = system.actorOf(Storage.props, "storage")

  val recorder = system.actorOf(Recorder.props(checker, storage), "recorder")

  val whitelistedUser = User("Sam", "sam.smith@example.com")
  val blacklistedUser = User("John", "john.doe@example.com")

  recorder ! AddUser(whitelistedUser)
  recorder ! AddUser(blacklistedUser)

  Thread.sleep(100)

  system.terminate()
}
