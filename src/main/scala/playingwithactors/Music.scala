package playingwithactors

import akka.actor.SupervisorStrategy.Stop
import akka.actor.{Actor, ActorSystem, Props}
import playingwithactors.MusicController.{Play, Stop}
import playingwithactors.MusicPlayer.{StartMusic, StopMusic}

/**
  * Created by andrejs.dasko on 19-Jun-18.
  */

object MusicController {
  def props = Props[MusicController]

  sealed trait ControllerMsg
  case object Play extends ControllerMsg
  case object Stop extends ControllerMsg
}

class MusicController extends Actor {
  override def receive: Receive = {
    case Play =>
      println("Music Started...")
    case MusicController.Stop =>
      println("Music stopped...")
    case _ =>
      println("Unknown message")
  }
}

object MusicPlayer {
  def props = Props[MusicPlayer]

  sealed trait PlayerMsg
  case object StartMusic extends PlayerMsg
  case object StopMusic extends PlayerMsg
}

class MusicPlayer extends Actor {
  override def receive: Receive = {
    case StartMusic => {
      val controller = context.actorOf(MusicController.props, "controller")
      controller ! Play
    }
    case StopMusic =>
      println("I don't want to stop the music")
    case _ =>
      println("Unknown message")
  }
}

object Music extends App {
  val system = ActorSystem("creation")

  val musicPlayerActor = system.actorOf(MusicPlayer.props, "music-player")

  musicPlayerActor ! StartMusic

  musicPlayerActor ! StopMusic

  system.terminate()
}
