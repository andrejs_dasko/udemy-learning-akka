import akka.actor.{Actor, ActorSystem, Props}

/**
  * Created by andrejs.dasko on 19-Jun-18.
  */
case class WhoToGreet(name: String)

class Greeter extends Actor {

  override def receive: Receive = {
    case WhoToGreet(name) => println(s"Hello, $name")
  }
}

object HelloAkka extends App {
  val system = ActorSystem("hello-akka")

  val greeterActor = system.actorOf(Props[Greeter], "greeter")

  greeterActor ! WhoToGreet("Akka")

  system.terminate()
}
