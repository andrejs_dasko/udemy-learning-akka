package supervisionandmonitoring

import akka.actor.SupervisorStrategy.{Escalate, Restart, Resume, Stop}
import akka.actor.{Actor, ActorRef, ActorSystem, OneForOneStrategy, Props, SupervisorStrategy}

/**
  * Created by andrejs.dasko on 19-Jun-18.
  */
object Aphrodite {
  case object ResumeException extends Exception
  case object RestartException extends Exception
  case object StopException extends Exception
}

class Aphrodite extends Actor {
  import Aphrodite._

  override def preStart(): Unit = {
    println("Aphrodite preStart hook...")
  }

  override def postStop(): Unit = {
    println("Aphrodite postStop hook ...")
  }

  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    println("Aphrodite preRestart hook...")
    super.preRestart(reason, message)
  }

  override def postRestart(reason: Throwable): Unit = {
    println("Aphrodite postRestart hook...")
    super.postRestart(reason)
  }

  override def receive: Receive = {
    case "Resume" =>
      throw ResumeException
    case "Restart" =>
      throw RestartException
    case "Stop" =>
      throw StopException
    case _ =>
      throw new Exception
  }
}

class Hera extends Actor {

  import Aphrodite._

  var childRef: ActorRef = _

  override val supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy() {
      case ResumeException => Resume
      case RestartException => Restart
      case StopException => Stop
      case _ => Escalate
    }

  override def preStart() = {
    childRef = context.actorOf(Props[Aphrodite], "aphrodite")
    Thread.sleep(100)
    super.preStart()
  }

  override def receive: Receive = {
    case msg =>
      println(s"Hera received $msg")
      childRef ! msg
      Thread.sleep(100)
  }
}

object Supervision extends App {
  val system = ActorSystem("supervision")

  val hera = system.actorOf(Props[Hera], "hera")

//  hera ! "Resume"
//  Thread.sleep(100)
//  println()

//  hera ! "Restart"
//  Thread.sleep(100)
//  println()

  hera ! "Stop"
  Thread.sleep(100)
  println()

  system.terminate()
}
