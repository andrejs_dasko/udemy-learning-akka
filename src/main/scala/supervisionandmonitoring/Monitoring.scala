package supervisionandmonitoring

import akka.actor.{Actor, ActorRef, ActorSystem, Props, Terminated}

/**
  * Created by andrejs.dasko on 19-Jun-18.
  */
class Ares(athena: ActorRef) extends Actor {

  override def preStart(): Unit = {
    println("Ares: preStart()")
    context.watch(athena)
  }

  override def postStop(): Unit = {
    println("Ares: postStop()")
  }

  override def receive = {
    case Terminated(`athena`) =>
      println("Ares: athena termination received. Stopping self.")
      context.stop(self)
  }
}

object Ares {
  def props(athena: ActorRef) = Props(new Ares(athena))
}

class Athena extends Actor {

  override def preStart(): Unit = {
    println("Athena: preStart()")
  }

  override def postStop(): Unit = {
    println("Athena: postStop()")
  }

  override def receive: Receive = {
    case msg =>
      println(s"Athena: Received message: $msg")
      println("Athena: Stopping self")
      context.stop(self)
  }
}

object Monitoring extends App {
  val system = ActorSystem("monitoring")

  val athena = system.actorOf(Props[Athena], "athena")

  val ares = system.actorOf(Ares.props(athena), "ares")

  athena ! "hello"

  Thread.sleep(3000)

  system.terminate()
}
