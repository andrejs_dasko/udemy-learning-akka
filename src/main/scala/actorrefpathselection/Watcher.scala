package actorrefpathselection

import akka.actor.{Actor, ActorIdentity, ActorRef, Identify}

/**
  * Created by andrejs.dasko on 20-Jun-18.
  */
class Watcher extends Actor {

  private val counterPath = "/user/counter"
  val counterSelection = context.actorSelection(counterPath)

  println(s"Calling Identify on $counterSelection")
  counterSelection ! Identify(None)

  override def receive: Receive = {
    case ActorIdentity(_, Some(actorRef)) =>
      println(s"Actor reference for path $counterPath is $actorRef")
    case ActorIdentity(_, None) =>
      println(s"Actor reference for path $counterPath does not exist")
  }
}
