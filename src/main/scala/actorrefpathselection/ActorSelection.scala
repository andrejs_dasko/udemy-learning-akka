package actorrefpathselection

import akka.actor.{ActorSystem, PoisonPill, Props}

/**
  * Created by andrejs.dasko on 20-Jun-18.
  */
object ActorSelection extends App {
  val system = ActorSystem("actor-selection")

  val counterActorRef1 = system.actorOf(Props[Counter], "counter")
  println(s"Counter ref1: $counterActorRef1")
  val counterSelection = system.actorSelection("counter")
  println(s"Counter selection1: $counterSelection")

  counterActorRef1 ! PoisonPill

  Thread.sleep(100)

  val counterActorRef2 = system.actorOf(Props[Counter], "counter")
  println(s"Counter ref2: $counterActorRef2")
  val counterSelection2 = system.actorSelection("counter")
  println(s"Counter selection2: $counterSelection2")

  system.terminate()
}
