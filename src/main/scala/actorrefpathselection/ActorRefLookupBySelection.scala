package actorrefpathselection

import akka.actor.{ActorPath, ActorRef, ActorSystem, Props}

/**
  * Created by andrejs.dasko on 20-Jun-18.
  */
object ActorRefLookupBySelection extends App {
  val system = ActorSystem("reference-lookup-by-selection")



  val watcherActor: ActorRef = system.actorOf(Props[Watcher], "watcher")
  Thread.sleep(100)

  val counterActor: ActorRef = system.actorOf(Props[Counter], "counter")

  val watcherActor2: ActorRef = system.actorOf(Props[Watcher], "watcher2")

  Thread.sleep(100)

  system.terminate()
}
