package actorrefpathselection

import akka.actor.Actor

/**
  * Created by andrejs.dasko on 20-Jun-18.
  */
object Counter {
  sealed trait CounterMsg
  case object Inc extends CounterMsg
  case object Dec extends CounterMsg
}

class Counter extends Actor {
  import Counter._

  override def preStart(): Unit = println("Counter started")

  var balance: Int = 0

  override def receive: Receive = {
    case Inc => balance += 1
    case Dec => balance -= 1
  }
}
